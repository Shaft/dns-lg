import requests

from data import servers, url

def test_default():
    response = requests.get("%s/www.bortzmeyer.org" % (url % servers["nobase"]["port"]))
    assert response.status_code == 200 and response.headers["content-type"].startswith("text/html")

def test_html():
    response = requests.get("%s/www.bortzmeyer.org?format=html" % (url % servers["nobase"]["port"]))
    assert response.status_code == 200 and response.headers["content-type"].startswith("text/html")
    response = requests.get("%s/www.bortzmeyer.org?format=HTML" % (url % servers["nobase"]["port"]))
    assert response.status_code == 200 and response.headers["content-type"].startswith("text/html")

def test_xml():
    response = requests.get("%s/www.bortzmeyer.org?format=xml" % (url % servers["nobase"]["port"]))
    assert response.headers["content-type"].startswith("application/xml")

def test_json():
    response = requests.get("%s/www.bortzmeyer.org?format=json" % (url % servers["nobase"]["port"]))
    assert response.headers["content-type"].startswith("application/json")

def test_gemini():
    response = requests.get("%s/www.bortzmeyer.org?format=gemini" % (url % servers["nobase"]["port"]))
    assert response.headers["content-type"].startswith("text/gemini")

def test_text():
    response = requests.get("%s/www.bortzmeyer.org?format=text" % (url % servers["nobase"]["port"]))
    assert response.headers["content-type"].startswith("text/plain")

def test_zone():
    response = requests.get("%s/www.bortzmeyer.org?format=zone" % (url % servers["nobase"]["port"]))
    assert response.headers["content-type"].startswith("text/dns")

def test_unknown():
    response = requests.get("%s/www.bortzmeyer.org?format=frobnicate" % (url % servers["nobase"]["port"]))
    assert response.status_code == 400 and response.headers["content-type"].startswith("text/plain")

    

