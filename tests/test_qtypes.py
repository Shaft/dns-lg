import requests

from data import servers, url

def test_default():
    response = requests.get("%s/dns.google.com" % (url % servers["nobase"]["port"]))
    assert response.status_code == 200 and "8.8.8.8" in response.text
    
def test_aaaa():
    response = requests.get("%s/dns.google.com/AAAA" % (url % servers["nobase"]["port"]))
    assert response.status_code == 200 and "::8888" in response.text
    
def test_ns():
    response = requests.get("%s/fr/NS" % (url % servers["nobase"]["port"]))
    assert response.status_code == 200 and "d.nic.fr" in response.text
    
def test_soa():
    response = requests.get("%s/fr/SOA" % (url % servers["nobase"]["port"]))
    assert response.status_code == 200 and "hostmaster.nic.fr." in response.text
    
def test_unknown():
    response = requests.get("%s/fr/XXXXXX" % (url % servers["nobase"]["port"]))
    assert response.status_code == 400 
    
