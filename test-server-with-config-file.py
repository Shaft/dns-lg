#!/usr/bin/env python3

import wsgiref.simple_server as server
import os
import getopt
import sys
import string
import configparser

config_file = os.path.expanduser("~/.dnslg.ini")

import DNSLG

default_values = {'email_administrator': '',
                  'url_documentation': '',
                  'url_css': '',
                  'url_base_service': DNSLG.default_base_url,
                  'file_favicon': '',
                  'forbidden_suffixes': "",
                  'encoding': DNSLG.default_encoding,
                  'size_edns': '2048',
                  'bucket_size': '10',
                  'handle_wellknown_files': 'True',
                  'code_google_webmasters': '',
                  'description': '',
                  'description_html': '',
                  'port': '8080'
                  }

def usage(msg=None):
    print("Usage: %s" % sys.argv[0], file=sys.stderr)
    if msg is not None:
        print(msg, file=sys.stderr)

if len(sys.argv) != 1:
    usage()
    sys.exit(1)

SECTION = "DNS-LG"
config = configparser.ConfigParser(default_values)
try:
    config_file = open(config_file)
except IOError:
    print("Cannot open configuration file %s" % config_file, file=sys.stderr)
    sys.exit(1)
config.readfp(config_file)
if not config.has_section(SECTION):
    config.add_section(SECTION)
email_admin = config.get(SECTION, 'email_administrator')
url_doc = config.get(SECTION, 'url_documentation')
url_css = config.get(SECTION, 'url_css')
file_favicon = config.get(SECTION, 'file_favicon')
base_url = config.get(SECTION, 'url_base_service')
port = config.getint(SECTION, 'port')
rl_bucket_size = config.getint(SECTION, 'bucket_size')
handle_wellknown_files = config.getboolean(SECTION, 'handle_wellknown_files')
description = config.get(SECTION, 'description')
description_html = config.get(SECTION, 'description_html')
google_code = config.get(SECTION, 'code_google_webmasters')
edns_size = config.get(SECTION, 'size_edns')
forbidden_str = config.get(SECTION, 'forbidden_suffixes')
forbidden = forbidden_str.split(':')
if edns_size is None or edns_size == "":
    edns_size = None
else:
    edns_size = int(edns_size) # TODO: handle conversion errors
encoding = config.get(SECTION, 'encoding')

querier = DNSLG.Querier(email_admin=email_admin, url_doc=url_doc, url_css=url_css,
                        base_url=base_url, file_favicon=file_favicon,
                        encoding=encoding,
                        edns_size=edns_size, bucket_size=rl_bucket_size,
                        google_code=google_code,
                        handle_wk_files=handle_wellknown_files,
                        description=description, description_html=description_html,
                        forbidden_suffixes=forbidden)
# TODO listen on IPv6 as well
httpd = server.make_server("", port, querier.application)
print("Serving HTTP on port %i..." % port)

# Respond to requests until process is killed
httpd.serve_forever()

